// top=cxx_with_clock::with_clock

#define TOP with_clock

#include <verilator_util.hpp>

// ANCHOR: clock_macro
#define TICK \
    dut->clk_i = 1; \
    ctx->timeInc(1); \
    dut->eval(); \
    dut->clk_i = 0; \
    ctx->timeInc(1); \
    dut->eval();
// ANCHOR_END: clock_macro

TEST_CASE(it_works, {

    // ANCHOR: clock_tick
    dut->clk_i = 1;
    ctx->timeInc(1);
    dut->eval();
    dut->clk_i = 0;
    ctx->timeInc(1);
    dut->eval();
    // ANCHOR_END: clock_tick

    // ANCHOR: using_clock_macro
    s.i->a = "5";
    s.i->b = "10";
    TICK;
    ASSERT_EQ(s.o, "15");
    // ANCHOR_END: using_clock_macro

    return 0;
})

MAIN
