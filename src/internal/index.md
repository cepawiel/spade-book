# Compiler Internals

This chapter describes some internals of the compiler and details about code generation.
Normally, this is not relevant to users of the language.
