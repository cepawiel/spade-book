# Type Declarations

## Struct

`struct` declaration include a name, optional generic arguments and a list of
fields. The fields in turn have a name and a type which may use the generic
arguments.

```spade
struct Empty {}

struct NonGeneric {
    field_a: int<8>,
    field_b: bool
}

struct Generic<T> {
    field_a: T,
    field_b: bool
}
```

## Enum

`enum` declarations also include a name and optional generic arguments. Their body
consists of a list of variants. Each variant in turn has a name, and an optional
list of fields

```spade
enum Enum {
    EmptyVariant,
    OneField{val: int<8>}
    TwoFields{val1: bool, val2: bool}
}

enum GenericEnum<T> {
    EmptyVariant,
    OneField{val: T}
}
