# Installation

At the moment, Spade is only well-supported on Linux systems, though some users
have had success running it on macOS as well. Windows is not supported for now,
though it should be usable in WSL.

In order to install Spade, you need the Rust toolchain, specifically `cargo`.
If you don't have it installed, you can install it with https://rustup.rs/.
Simply run the command there, make sure its binaries are in your `PATH` and
then run `rustup toolchain install stable`

Unless you have specific needs, you should install the Spade compiler via its
build tool Swim. Swim then manages the compiler version on a per-project basis.
To install Swim, run
```bash
cargo install --git https://gitlab.com/spade-lang/swim
```

If you are on arch-linux you can also install the `swim-git` package from the
aur https://aur.archlinux.org/packages/swim-git

You should now be able to create a swim project using `swim init hello_world`


## Synthesis Tools and Simulators

Spade compiles to Verilog code which is simulated and synthesised (compiled to
hardware) by other tools. In particular cocotb for simulation, and yosys+nextpnr for synthesis.

### Automated install

The easiest way to install those tools is via Swim. This is done by running
```
swim install-tools
```
which downloads https://github.com/YosysHQ/oss-cad-suite-build into
`~/.local/share/swim`. If it is installed, swim will always use the cad-suite tools instead of system tools.

> NOTE: If you need to uninstall those tools, remove `~/.local/share/swim/bin`

### Manual install

You can also install the tools manually. Refer to the individual installation instructions in that case. The tools you need are:

- Python3.8 or later
- Simulation via Icarus Verilog (using cocotb): https://github.com/steveicarus/iverilog
- Simulation via Verilator: https://github.com/verilator/verilator
- Synthesis: https://yosyshq.net/yosys/
- Place and route: https://github.com/YosysHQ/nextpnr. Build nextpnr-ice40 or nextpnr-ecp5 depending on your target FPGA
- Bitstream generation and upload. Depends on your target FPGA
- Cocotb: https://www.cocotb.org/ (this automatically is installed for you by swim)

> If you're just starting out, you probably don't need all of these. Start of
> by simulating your designs using `cocotb` and `icarus`, then you can move on
> to testing in hardware using `yosys` and `nextpnr`.
>
> If your simulations are too slow, you can try `verilator`
